Goal
-----------------------------------------------------------------------------------------------------------------------------------
The goal of the project is, if you have no internet connection on a computer with gentoo, you can download these files on
another computer with an internet connection.

If you want to use our downloader, look to our github-repository and our Wiki-Page:
https://github.com/sikienzl/GentooOfflinePackageDownloader/wiki
