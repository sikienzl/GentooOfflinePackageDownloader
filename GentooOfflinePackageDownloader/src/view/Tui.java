package view;

import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import controller.Controller;

public class Tui {
	private static final Logger logger = LogManager.getLogger("Tui");
	private Scanner scanner;
	private Controller controller;
	
	public Tui()
	{
		controller = new Controller();
	}
	 
	public void printTui()
	{
		logger.info("Welcome to the GentooOfflinePackageDownloader!");
		logger.info("Choose one option:");
		logger.info("1. Download files from *.txt-File");
		logger.info("2. Exit");
	}
	public boolean processInputLine(String line){
        boolean continu = false;
        if (line.equalsIgnoreCase("1")) {
            if(printSourceQuestion())
            {
            	if(printDestinationQuestion())
            	{
            		continu = true;
            	}
            }
        } 
        if (line.equalsIgnoreCase("2")) {
            continu = false;
        }
        return continu;
	}
	
	public boolean printSourceQuestion()
	{
		boolean existFileFlag = false;
		logger.info("Where is the file with the list of downloads saved?");
		scanner = new Scanner(System.in);
		existFileFlag = controller.existSourcefile(scanner.next());
		if(existFileFlag == false)
		{
			logger.error("File not exist!");
			
		}
		
		
		return existFileFlag; 
	}
	public boolean printDestinationQuestion()
	{
		boolean existPathFlag = false;
		logger.info("Where would you like to safe the files?");
		scanner = new Scanner(System.in);
		existPathFlag = controller.existDestinationPath(scanner.next());
		if(existPathFlag == false)
		{
			logger.error("Path not exist or is not a Directory!");
		}
		return existPathFlag;
	}
	public void printDownload()
	{
		logger.info("Download starts...");
		controller.downloadFiles();
	}

}
