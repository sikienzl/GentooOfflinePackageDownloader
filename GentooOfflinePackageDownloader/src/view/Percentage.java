package view;

public class Percentage {
	
	public int percent(int currentCount, int countFiles)
	{
		return (currentCount * 100) / countFiles;
	}
}
