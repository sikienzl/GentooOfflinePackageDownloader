package extractor;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.unix4j.*;
import org.unix4j.unix.Cut;

public class UrlExtractor {
	
	public List<String> extractUrls(String file)
	{
		
		String allURLs = Unix4j.cat(file).cut(Cut.Options.f," ",1).grep("^http://").uniq().toStringResult() + " ";
		/*split string on space*/
		String[] urlsArray = allURLs.split("\\s+");
		return new LinkedList<>(Arrays.asList(urlsArray));
	}
}
