package downloader;


import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class Downloader {
	/*10 seconds for timeout*/
	private int connectionTimeout = 10000;
	private int readTimeout = 10000;
	private static final Logger logger = LogManager.getLogger("Downloader");
	
	
	public void startDownload(String source, String destinationFolder)
	{
		URL sourceURL = getUrl(source);
		download(sourceURL, getDestinationString(sourceURL, destinationFolder));
	}
	
	private URL getUrl(String source) 
	{
		try
		{
			return new URL(source);
		} 
		catch(MalformedURLException mEx)
		{
			logger.error(mEx.toString());
			return null;
		}
	}
	
	private String getDestinationString(URL sourceURL, String destination)
	{
		String downloadFile = extractFileFromUrl(sourceURL);
		String destinationPath = "";
		destinationPath = destination + File.separator + downloadFile;
		return destinationPath;
	}
	
	private void download(URL sourceURL, String destination)
	{
		try
		{
			FileUtils.copyURLToFile(sourceURL, new File(destination), connectionTimeout, readTimeout);
		} catch(IOException e)
		{
			logger.error(e.toString());
		}
	}
	
	public String extractFileFromUrl(URL source)
	{
		String file = ""; 
		file = FilenameUtils.getName(source.getPath());
		return file;
	}
}
