package controller;

import java.io.File;
import java.util.List;

import downloader.Downloader;
import extractor.UrlExtractor;
import view.Percentage;

public class Controller {
	
	/*	UrlExtractor urlExtractor = new UrlExtractor();
	List<String> urls = urlExtractor.extractUrls("D:\\downloads\\test\\emerge.txt");
	Downloader download = new Downloader();
	int currentCount = 0;
	int currentPercent = 0;
	Percentage percentage = new Percentage();
	for(String url : urls)
	{
		++currentCount;
		currentPercent = percentage.percent(currentCount, urls.size());
		logger.info(currentPercent + " % current download: " + url);
		download.startDownload(url, "D:\\downloads");
		
	}*/
	
	private List<String> urls;
	private String sourcefile;
	private String destinationpath;
		
	public boolean existSourcefile(String sourcefile)
	{
		boolean existSourceFile = false;
		
		File f = new File(sourcefile);
		if(f.exists() && !f.isDirectory()) { 
		    existSourceFile = true;
		    setSourcefile(sourcefile);
		}
		
		return existSourceFile;
	}
	
	public boolean existDestinationPath(String destinationpath)
	{
		boolean existPath = false;
		
		File f = new File(destinationpath);
		if(f.exists() && f.isDirectory()) { 
		    existPath = true;
		    setDestinationPath(destinationpath);
		}
		
		return existPath;
	}
	
	public void setSourcefile(String sourcefile)
	{
		this.sourcefile = sourcefile;
	}
	public String getSourcefile()
	{
		return sourcefile;
	}
	public void setDestinationPath(String destinationpath)
	{
		this.destinationpath = destinationpath;
	}
	public String getDestinationPath()
	{
		return destinationpath;
	}
	public void extractUrl(String pathfile)
	{
		UrlExtractor urlExtractor = new UrlExtractor();
		urls = urlExtractor.extractUrls(pathfile);
	}
	public void downloadFiles()
	{
		//int currentCount = 0;
		//int currentPercent = 0;
		//Percentage percentage = new Percentage();
		extractUrl(getSourcefile());
		Downloader download = new Downloader();
		
		for(String url : urls)
		{
			//++currentCount;
			//currentPercent = percentage.percent(currentCount, urls.size());
			//logger.info(currentPercent + " % current download: " + url);
			download.startDownload(url, getDestinationPath());
			
		}
	}
}
