package main;

import java.util.Scanner;

import view.Tui;

public class GentooOfflinePackageDownloader {
	private static boolean continueFlag = true;
	private static Scanner scanner;
	
	public static void main(String[] args) {
		Tui tui = new Tui();
		scanner = new Scanner(System.in);
		do
		{
			tui.printTui();
			continueFlag = tui.processInputLine(scanner.next()); 
			if(continueFlag)
			{
				tui.printDownload();
			}
			
			
		} while(continueFlag);
	}

}
