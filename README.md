# GentooOfflinePackageDownloader
a downloader for Gentoo distfiles

## Instruction
If you want to download all distfiles you needed to rebuild your machine without internet, run first 

```(emerge -pevf system && emerge -pevf world) 2>&1 > <filename>.txt``` on this computer.

Copy this file on a usb-drive and go to a computer with internet.

Put the usb-drive with this file in the computer with internet, and copy this file into a folder.

Run the GentooOfflinePackageDownloader

Then you copy the files from the destination-path in your usb-drive. 

Go back to your computer with gentoo and without internet and copy these files into 

```/usr/portage/distfiles```

After that run 

```emerge -avuND world```
